package popschool.mafia_spring_rest_client_hateaos.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.RemoteResource;

import java.util.List;

@Data
@RemoteResource("/jobs")
public class Job {

    private Long job_id;

    private String job_name;

    private int score;

    private Double setupcost;

    private List<Affectation> gangsters;


}
