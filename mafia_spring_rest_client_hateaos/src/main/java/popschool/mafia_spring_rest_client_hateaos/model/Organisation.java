package popschool.mafia_spring_rest_client_hateaos.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.List;

@Data
@RemoteResource("/organisations")
public class Organisation {

    private URI orgid;

    private String orgname;

    private String description;

    private Gangster boss;

    private List<Gangster> gangsters;

    @Override
    public String toString() {
        return "Organisation{" +
                "org_name='" + getOrgname() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", boss=" + getBoss() +
                '}';
    }

    @ResourceId
    public URI getOrgid(){
        return orgid;
    }

    public Gangster getBoss(){
        return boss;
    }


}
