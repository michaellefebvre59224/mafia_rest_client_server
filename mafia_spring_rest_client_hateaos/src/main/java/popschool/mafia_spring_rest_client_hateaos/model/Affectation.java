package popschool.mafia_spring_rest_client_hateaos.model;

import lombok.Data;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

@Data
@RemoteResource("/affectations")
public class Affectation {

    private Long affectation_id;

    private Gangster gangster;

    private Job job;



}
