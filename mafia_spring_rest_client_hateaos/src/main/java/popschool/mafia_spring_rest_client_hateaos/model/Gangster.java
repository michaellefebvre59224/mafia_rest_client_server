package popschool.mafia_spring_rest_client_hateaos.model;

import com.sun.org.apache.xpath.internal.operations.Or;
import lombok.Data;
import uk.co.blackpepper.bowman.annotation.LinkedResource;
import uk.co.blackpepper.bowman.annotation.RemoteResource;
import uk.co.blackpepper.bowman.annotation.ResourceId;

import java.net.URI;
import java.util.List;

@Data
@RemoteResource("/gangsters")
public class Gangster {

    private URI gangster_id;

    private String gname;

    private String nickname;

    private int badness;


    private List<Affectation> jobs;

    private Organisation organisation;

    private Organisation organisationGere;

    @LinkedResource
    public Organisation getOrganisation(){
        return organisation;
    }

    @ResourceId
    public URI getGangster_id() {
        return gangster_id;
    }

    @LinkedResource
    public Organisation getOrganisationGere(){
        return organisationGere;
    }

    public Gangster(String gname, String nickname, int badness) {
        this.gname = gname;
        this.nickname = nickname;
        this.badness = badness;
    }

    public Gangster() {
    }
}
