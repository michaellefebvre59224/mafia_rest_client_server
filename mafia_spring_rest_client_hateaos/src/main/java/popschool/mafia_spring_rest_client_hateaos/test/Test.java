package popschool.mafia_spring_rest_client_hateaos.test;

import com.sun.org.apache.xml.internal.resolver.readers.OASISXMLCatalogReader;
import org.springframework.beans.factory.annotation.Autowired;
import popschool.mafia_spring_rest_client_hateaos.model.Gangster;
import popschool.mafia_spring_rest_client_hateaos.model.Organisation;
import popschool.mafia_spring_rest_client_hateaos.service.CinemaService;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.net.URL;
import java.util.Optional;

public class Test {

    public static void main(String[] args) {

        String URL_GANGSTER = "http://localhost:8081/gangster";

        ClientFactory clientFactory = Configuration.builder().build().buildClientFactory();;

        Client<Gangster> client = clientFactory.create(Gangster.class);
        Iterable<Gangster> gangsters = client.getAll(URI.create("http://localhost:8081/gangsters"));

        System.out.println("//////////////////////////////////////////////////////");
        System.out.println("////////          GANGSTER     GETALL          ///////");
        System.out.println("//////////////////////////////////////////////////////");
        System.out.println(gangsters);

        Gangster gangster = new Gangster("test", "test", 5);


        Client<Organisation> clientOrg = clientFactory.create(Organisation.class);
        Organisation organisation = clientOrg.get(URI.create("http://localhost:8081/organisations/search/findByOrgname?name=Yakuza"));

        System.out.println("//////////////////////////////////////////////////////");
        System.out.println("////////          ORGANISATION     GET          ///////");
        System.out.println("//////////////////////////////////////////////////////");
        System.out.println(organisation);



    }


}
