package popschool.mafia_spring_rest_client_hateaos.service;

import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.stereotype.Service;
import popschool.mafia_spring_rest_client_hateaos.model.Gangster;
import popschool.mafia_spring_rest_client_hateaos.model.Organisation;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.util.List;

@Service
public class CinemaServiceImp implements CinemaService {

    final String URL_GANGSTER = "http://localhost:8081/gangsters";
    final String URL_ORGANISATION = "http://localhost:8081/organisations";
    final String URL_GANG = "http://localhost:8081";

    ClientFactory clientFactory = Configuration.builder().build().buildClientFactory();
    ClientFactory clientFactory2 = Configuration.builder().setBaseUri(URL_GANG).build().buildClientFactory();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  GANGSTER
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public Iterable<Gangster> getAllGangster() {
        Client<Gangster> client = clientFactory.create(Gangster.class);
        Iterable<Gangster> gangsters = client.getAll(URI.create(URL_GANGSTER));
        return gangsters;
    }

    @Override
    public void createGanster(Gangster gangster) {
        Client<Gangster> client = clientFactory2.create(Gangster.class);
        URI id = client.post(gangster);
    }

    @Override
    public void putGanster(Gangster gangster) {
        Client<Gangster> client = clientFactory2.create(Gangster.class);
        client.put(gangster);
    }

    @Override
    public Gangster getGangsterByName(String name) {
        Client<Gangster> client = clientFactory.create(Gangster.class);
        Gangster gangster = client.get(URI.create("http://localhost:8081/gangsters/search/findByGname?name=" + name));
        return gangster;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  ORGANISATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Iterable<Organisation> getAllOrganisation() {
        Client<Organisation> client = clientFactory.create(Organisation.class);
        Iterable<Organisation> organisations = client.getAll(URI.create(URL_ORGANISATION));
        return organisations;
    }

    @Override
    public Organisation getOrganisationByName(String name) {
        Client<Organisation> client = clientFactory.create(Organisation.class);
        Organisation organisation = client.get(URI.create("http://localhost:8081/organisations/search/findByOrgname?name=" + name ));
        return organisation;
    }


}
