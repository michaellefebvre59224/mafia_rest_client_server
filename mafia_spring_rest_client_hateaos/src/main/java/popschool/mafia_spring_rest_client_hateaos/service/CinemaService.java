package popschool.mafia_spring_rest_client_hateaos.service;

import org.springframework.stereotype.Service;
import popschool.mafia_spring_rest_client_hateaos.model.Gangster;
import popschool.mafia_spring_rest_client_hateaos.model.Organisation;

import java.util.List;


public interface CinemaService {
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  GANGSTER
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Iterable<Gangster> getAllGangster();
    void createGanster(Gangster gangster);
    void putGanster(Gangster gangster);
    Gangster getGangsterByName(String name);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                  ORGANISATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Iterable<Organisation> getAllOrganisation();
    Organisation getOrganisationByName(String name);

}
