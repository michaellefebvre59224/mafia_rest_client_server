package popschool.mafia_spring_rest_client_hateaos.controler;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.mafia_spring_rest_client_hateaos.model.Gangster;
import popschool.mafia_spring_rest_client_hateaos.model.Organisation;
import popschool.mafia_spring_rest_client_hateaos.service.CinemaService;
import uk.co.blackpepper.bowman.Client;
import uk.co.blackpepper.bowman.ClientFactory;
import uk.co.blackpepper.bowman.Configuration;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

@Controller
@SessionAttributes(value = "gangsterSession", types={Gangster.class})
public class controler {

    @Autowired
    private CinemaService cinemaService;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          METHODE SESSION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @ModelAttribute("gangsterSession")
    public Gangster addMyBean1ToSessionScope() {
        return new Gangster();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          METHODE changement page
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping(value = "/listeGangster")
    public String pageListGangster(Model model){
        Gangster newGangster = new Gangster();
        Iterable<Gangster> gangsters = cinemaService.getAllGangster();
        model.addAttribute("listGangster", gangsters);
        model.addAttribute("newGangster", newGangster);
        return "pageListGangster";
    }

    @PostMapping("/newGangster")
    public String newGangster(@ModelAttribute("newGangster") Gangster newGangster, Model model){
        try{
            cinemaService.createGanster(newGangster);
            return "redirect:/listeGangster";
        }catch (Exception e){
            return "redirect:/listeGangster";
        }
    }

    @GetMapping(value = "/detailGangster/{name}")
    public String pageDetailGangster(@PathVariable("name")String name,
                                     @ModelAttribute("gangsterSession") Gangster gangsterSession,
                                     Model model){
        String organisationSelect = "";
        System.out.println(name);
        Gangster gangster = cinemaService.getGangsterByName(name);
        Iterable<Organisation> organisations =cinemaService.getAllOrganisation();
        gangsterSession = gangster;
        model.addAttribute("gangsterSession", gangsterSession);
        model.addAttribute("gangster", gangster);
        model.addAttribute("organisations", organisations);
        model.addAttribute("organisationSelect", organisationSelect);
        return "pageDetailGangster";
    }

    @PostMapping("/ajouterMembreOrganisation")
    public String membreOrganisation(@ModelAttribute("organisationSelect") String name,
                                     @ModelAttribute("gangsterSession") Gangster gangsterSession,
                                     Model model){
        System.out.println(gangsterSession.getGname());
        System.out.println(name);
        try{
            Organisation organisationSelect = cinemaService.getOrganisationByName(name);
            gangsterSession.setOrganisation(organisationSelect);
            cinemaService.putGanster(gangsterSession);
            return "redirect:/detailGangster/{name}(name=${gangsterSession.getGname()})";
        }catch (Exception e){
            return "redirect:/detailGangster/{name}(name=${gangsterSession.getGname()})";
        }
    }

}
