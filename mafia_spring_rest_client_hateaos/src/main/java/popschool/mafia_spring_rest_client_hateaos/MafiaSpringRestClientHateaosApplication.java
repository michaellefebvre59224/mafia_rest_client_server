package popschool.mafia_spring_rest_client_hateaos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MafiaSpringRestClientHateaosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MafiaSpringRestClientHateaosApplication.class, args);
    }

}
