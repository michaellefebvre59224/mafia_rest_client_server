package popschool.mafia_spring_rest_server_hateaos.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "JOB_ID")
    private Long job_id;
    @Column(name = "JOB_NAME")
    private String job_name;
    @Column(name = "SCORE")
    private int score;
    @Column(name = "SETUPCOST")
    private Double setupcost;

    @OneToMany(mappedBy = "job")
    private List<Affectation> gangsters;


}
