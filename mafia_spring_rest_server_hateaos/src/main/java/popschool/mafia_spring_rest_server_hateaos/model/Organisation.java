package popschool.mafia_spring_rest_server_hateaos.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Organisation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORG_ID")
    private Long orgid;

    @Column(name = "ORG_NAME")
    private String orgname;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToOne
    @JoinColumn(name = "THEBOSS")
    private Gangster boss;

    @OneToMany(mappedBy = "organisation")
    private List<Gangster> gangsters;

}
