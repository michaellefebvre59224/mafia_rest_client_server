package popschool.mafia_spring_rest_server_hateaos.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Gangster {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "GANGSTER_ID")
    private Long gangster_id;

    @Column(name = "GNAME")
    private String gname;
    @Column(name = "NICKNAME")
    private String nickname;
    @Column(name = "BADNESS")
    private int badness;

    @OneToMany(mappedBy = "gangster")
    private List<Affectation> jobs;

    @ManyToOne
    @JoinColumn(name = "ORG_ID")
    private Organisation organisation;

    @OneToOne(mappedBy = "boss")
    private Organisation organisationGere;


}
