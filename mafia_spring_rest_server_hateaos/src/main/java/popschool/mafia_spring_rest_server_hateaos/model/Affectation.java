package popschool.mafia_spring_rest_server_hateaos.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Affectation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AFFECTATION_ID")
    private Long affectation_id;

    @ManyToOne
    @JoinColumn(name = "GANGSTER_ID")
    private Gangster gangster;

    @ManyToOne
    @JoinColumn(name = "JOB_ID")
    private Job job;



}
