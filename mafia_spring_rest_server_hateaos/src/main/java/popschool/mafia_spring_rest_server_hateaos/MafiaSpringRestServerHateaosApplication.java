package popschool.mafia_spring_rest_server_hateaos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MafiaSpringRestServerHateaosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MafiaSpringRestServerHateaosApplication.class, args);
    }

}
