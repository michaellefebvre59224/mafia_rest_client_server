package popschool.mafia_spring_rest_server_hateaos.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.mafia_spring_rest_server_hateaos.model.Affectation;

@RepositoryRestResource(collectionResourceRel = "affectations", path = "affectations")
public interface AffectationRepo extends PagingAndSortingRepository<Affectation, Long> {

}
