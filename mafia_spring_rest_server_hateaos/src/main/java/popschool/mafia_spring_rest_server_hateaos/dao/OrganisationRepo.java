package popschool.mafia_spring_rest_server_hateaos.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.mafia_spring_rest_server_hateaos.model.Gangster;
import popschool.mafia_spring_rest_server_hateaos.model.Job;
import popschool.mafia_spring_rest_server_hateaos.model.Organisation;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "organisations", path = "organisations")
public interface OrganisationRepo extends PagingAndSortingRepository<Organisation, String> {

    Optional<Organisation> findByOrgname(String name);

}
