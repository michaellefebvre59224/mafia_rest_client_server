package popschool.mafia_spring_rest_server_hateaos.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import popschool.mafia_spring_rest_server_hateaos.model.Affectation;
import popschool.mafia_spring_rest_server_hateaos.model.Gangster;
import popschool.mafia_spring_rest_server_hateaos.model.Organisation;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "gangsters", path = "gangsters")
public interface GangsterRepo extends PagingAndSortingRepository<Gangster, Long> {
    Optional<Gangster> findByGname(String name);

}
